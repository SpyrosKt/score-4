
/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

public class StrictHeuristic {
	
	static final int TERMINAL_VALUE = 1000000000;
	static final int MAX_QUAD_VALUE = (int) Math.pow(4, 11);
	
	
	/**
	 * Evaluate all the board
	 * @param board
	 * @return the evaluation of the board
	 */
	public static int evaluate(Board board) {
		
		if(board.becameTerminal())
			return TERMINAL_VALUE * board.getLastPlayer().getVal();
		
		int boardValue = 0;
		int cellsValue;
		for(int i = 0; i < Board.ROWS; i++) {
			for(int j = 0; j < Board.COLS; j++) {
				
				// checks if the currently inspected cells can become a quad in the next turn
				// if so, the value returned is the maximum possible
				
				cellsValue = evaluateCells(board.getNorth(i, j));
				if(cellsValue == MAX_QUAD_VALUE / 4 * board.getCurrentPlayer().getVal())
					return TERMINAL_VALUE * board.getCurrentPlayer().getVal();
				else
					boardValue += cellsValue;
				
				cellsValue = evaluateCells(board.getNorthWest(i, j));
				if(cellsValue == MAX_QUAD_VALUE / 4 * board.getCurrentPlayer().getVal())
					return TERMINAL_VALUE * board.getCurrentPlayer().getVal();
				else
					boardValue += cellsValue;
				
				cellsValue = evaluateCells(board.getWest(i, j));
				if(cellsValue == MAX_QUAD_VALUE / 4 * board.getCurrentPlayer().getVal())
					return TERMINAL_VALUE * board.getCurrentPlayer().getVal();
				else
					boardValue += cellsValue;
				
				cellsValue = evaluateCells(board.getSouthWest(i, j));
				if(cellsValue == MAX_QUAD_VALUE / 4 * board.getCurrentPlayer().getVal())
					return TERMINAL_VALUE * board.getCurrentPlayer().getVal();
				else
					boardValue += cellsValue;

			}
		}
		
		return boardValue;
	}
	
	/**
	 * Evaluates the cells that are given. If the cell has a disc, its value becomes 4^11 or -4^11 if its opponent's disc.
	 * If the cell is empty its value is divided by 4 and is divided again depending on its support.
	 * @param 2-dimension array, the first dimension holds the context of the cells, 
	 * the second one, if the cell is empty holds the number of the spaces underneath it 
	 */
	public static int evaluateCells(int[][] cells) {
		
		if(cells == null) return 0;
		
		// again, starts by finding who "owns" the four tiles
		Player owner = null;
		for(int i = 0; i < 4; i++)
			if(owner == null)
				owner = Player.getPlayer(cells[i][0]);
			else
				if(Player.getPlayer(cells[i][0]) != null && Player.getPlayer(cells[i][0]) != owner)
					return 0;
		
		if(owner == null) return 0;
		
		// ensures that the worst possible block of 4 tiles will get 2 points
		int value = MAX_QUAD_VALUE * owner.getVal();
		
		for(int i = 0; i < 4; i++)
			if(cells[i][0] == 0) {
				value /= 4;
				value /= Math.pow(2, cells[i][1]);
			}
		
		return value;
		
	}
	
}

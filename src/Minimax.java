
/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

import java.util.ArrayList;

public class Minimax {
	
	private static int depth = 5;
	
	
	public static Board minimax(Board board) {
		if(board.isFull()) return board;
		return max(board, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	private static Board max(Board board, int level, int a, int b) {
		
		int v = Integer.MIN_VALUE;
		
		ArrayList<Board> children = board.getChildren();
		level++;
		
		for(int i = 0; i < children.size(); i++) {
			
			Board current = children.get(i);
			
			// if the child is a terminal state, min should not be called for it
			if(current.becameTerminal()) {
				// a terminal state can have a bit higher value than another by being "higher" on the tree
				current.setValue((StrictHeuristic.TERMINAL_VALUE + 10 - level) * current.getLastPlayer().getVal());
				// having found a terminal state, max doesn't need to search for better options
				break;
			}
			else if(level == depth || current.isFull()) {
				current.evaluate();									
			}
			else
				min(current, level, a, b);
			
			
			if(current.getValue() > v)
				v = current.getValue();
			if(v >= b) {
				while(children.size() - 1 > i)
					children.remove(i + 1);
			}
			if(v > a)
				a = v;
			
		}
		
		
		int max = children.get(0).getValue();
		int maxIndex = 0;
		for(int i = 1; i < children.size(); i++)
			if(children.get(i).getValue() > max) {
				max = children.get(i).getValue();					
				maxIndex = i;
			}
		
		board.setValue(max);			
		return children.get(maxIndex);
		
	}
	
	private static void min(Board board, int level, int a, int b) {
		
		int v = Integer.MAX_VALUE;
		
		ArrayList<Board> children = board.getChildren();
		level++;
		
		for(int i = 0; i < children.size(); i++) {	
			
			Board current = children.get(i);
			
			if(current.becameTerminal()){
				// a terminal state can have a bit higher value than another by being "higher" on the tree
				current.setValue((StrictHeuristic.TERMINAL_VALUE + 10 - level) * current.getLastPlayer().getVal());
				// having found a terminal state, min doesn't need to search for better options
				break;
			}
			else if (level == depth || current.isFull())
				current.evaluate();
			else
				max(current, level, a, b);
			
			
			if(current.getValue() < v)
				v = current.getValue();
			
			if(v <= a) {
				while(children.size() - 1 > i)
					children.remove(i + 1);
			}
			
			if(v < b)
				b = v;
			
		}
		
		
		int min = children.get(0).getValue();
		
		for(int i = 1; i < children.size(); i++) {
			if(children.get(i).getValue() < min)
				min = children.get(i).getValue();
		}
		
		board.setValue(min);
		
	}
	
	public static void setDepth(int depth) {
		Minimax.depth = depth;
	}
	
}


/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI extends JFrame {
	
	private static final long serialVersionUID = 1L;	
	
	private Board board;
	private JLayeredPane boardPanel;
	private JLabel[][] boardCells = new JLabel[6][7];
	private JPanel buttonPanel;
	private JButton[] buttons = new JButton[7];
	private CustomMouseAdapter[] adapters = new CustomMouseAdapter[7];
	
	private ImageIcon icon = new ImageIcon(getClass().getResource("icon.png").getPath());
	private ImageIcon redDisc = new ImageIcon(getClass().getResource("redDiscSmall.png").getPath());
	private ImageIcon yellowDisc = new ImageIcon(getClass().getResource("yellowDiscSmall.png").getPath());
	private ImageIcon grayDisc = new ImageIcon(getClass().getResource("grayDiscSmall.png").getPath());
	private ImageIcon redDiscT = new ImageIcon(getClass().getResource("redDiscSmallT.png").getPath());
	private ImageIcon yellowDiscT = new ImageIcon(getClass().getResource("yellowDiscSmallT.png").getPath());
	private ImageIcon playerDisc, playerDiscT, computerDisc;
	private ImageIcon boardImage = new ImageIcon(getClass().getResource("TableSh.png").getPath());
	
	
	private class CustomMouseAdapter extends MouseAdapter {
		
		private JButton button;
		private int buttonColumn;
		private boolean enabled;
		
		public CustomMouseAdapter(int a) {
			this.button = buttons[a];
			this.buttonColumn = a;
			this.enabled = true;
		}
		
		public void mouseEntered(MouseEvent evt) {
			if (board.getEmptyCells()[buttonColumn] != 0 && enabled) {
				button.setIcon(playerDisc);
				boardCells[board.getEmptyCells()[buttonColumn] - 1][buttonColumn].setIcon(playerDiscT);
			} else
				button.setIcon(grayDisc);
		}
		
		public void mouseExited(MouseEvent evt) {
			button.setIcon(null);
			if (board.getEmptyCells()[buttonColumn] != 0 && enabled)
				boardCells[board.getEmptyCells()[buttonColumn] - 1][buttonColumn].setIcon(null);
			else
				button.setIcon(null);
		}
		
		public void mouseClicked(MouseEvent evt) {
			if (board.getEmptyCells()[buttonColumn] != 0 && enabled) {
				
				boardCells[board.getEmptyCells()[buttonColumn] - 1][buttonColumn].setIcon(playerDisc);
				board.insertAt(buttonColumn);
			
				//show win message
				if (board.becameTerminal()) {
					disableButtons();
					showMessage(Player.HUMAN);
				}			
				//show draw message
				else if(board.isFull()) {				
					showMessage(null);
				}
				else {
					
					int move = Minimax.minimax(board).getLastMoveCol();
					boardCells[board.getEmptyCells()[move] - 1][move].setIcon(computerDisc);
					board.insertAt(move);
					
					// show loss message
					if (board.becameTerminal()) {
						disableButtons();					
						showMessage(Player.COMPUTER);
					}
					
				}
				
				// show draw message
				if(board.isFull())				
					showMessage(null);
				
				button.setIcon(playerDisc);
				
				if (board.getEmptyCells()[buttonColumn] != 0 && enabled)
					boardCells[board.getEmptyCells()[buttonColumn] - 1][buttonColumn].setIcon(playerDiscT);
				else
					button.setIcon(grayDisc);
				
			}
			
		}
		
		public void disable() {
			this.enabled = false;
		}
		
	}
	
	public GUI(Board board) {
		
		this.board = board;
		
		initComponents();
		
		if (board.getCurrentPlayer() == Player.COMPUTER) {
			int move = Minimax.minimax(board).getLastMoveCol();
			board.insertAt(move);
			boardCells[board.getEmptyCells()[move]][move].setIcon(computerDisc);
		}
		
		this.setVisible(true);
		
	}
	
	public Board getBoard() {
		return board;
	}
	
	private void initComponents() {
		
		// select a random color for the player
		double r = Math.random();
		if (r > 0.5) {
			playerDisc = redDisc;
			playerDiscT = redDiscT;
			computerDisc = yellowDisc;
		}
		else {
			playerDisc = yellowDisc;
			playerDiscT = yellowDiscT;
			computerDisc = redDisc;
		}
		
		
		buttonPanel = new JPanel();
		
		for (int i = 0; i < Board.COLS; i++)
			buttons[i] = new JButton();
		
		boardPanel = new JLayeredPane();
		
		for (int i = 0; i < Board.ROWS; i++)
			for (int j = 0; j < Board.COLS; j++)
				boardCells[i][j] = new JLabel();
			
		setName("Score 4");
		setTitle("Score 4");
		setIconImage(icon.getImage());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(false);
		
		boardPanel.setMaximumSize(new Dimension(720, 730));
		boardPanel.setMinimumSize(new Dimension(720, 730));
		boardPanel.setPreferredSize(new Dimension(720, 730));
		
		for (int i = 0; i < 7; i++) {
			buttons[i].setSize(100, 100);
			buttons[i].setOpaque(false);
			buttons[i].setContentAreaFilled(false);
			buttons[i].setBorderPainted(false);
			adapters[i] = new CustomMouseAdapter(i);
			buttons[i].addMouseListener(adapters[i]);
			buttons[i].setBounds(i * 100 + 10, 0, 100, 100);
			boardPanel.add(buttons[i], new Integer(1));
		}
		
		boardPanel.add(buttonPanel, new Integer(1));
		
		for (int i = 0; i < Board.ROWS; i++) {
			for (int j = 0; j < Board.COLS; j++) {
				boardCells[i][j].setBounds(j * 100 + 10, i * 100 + 120, 100, 100);
				boardPanel.add(boardCells[i][j], new Integer(1));
			}
		}
		
		JLabel t = new JLabel(boardImage);
		t.setBounds(0, 110, 720, 620);
		boardPanel.add(t, new Integer(2));
		
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		
		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(10)
					.addComponent(boardPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(10)
				)
		);
		
		layout.setVerticalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addGap(10)
					.addComponent(boardPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
				)
		);
		
		
		pack();
		
		setLocationRelativeTo(null);
		
	}
	
	private void disableButtons() {
		for(int i = 0; i < 7; i++) {
			adapters[i].disable();
		}
	}
	
	public void showMessage(Player winner) {
		new PopupMessage(winner, this);
	}
	
}

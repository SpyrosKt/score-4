
/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

public class MainApp {

	public static void main(String args[]) {
		
		Minimax.setDepth(3);
		Board board = new Board();
		new GUI(board);
		
	}
	
}


/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

import javax.swing.*;

public class PopupMessage extends JFrame {
	
	private static final long serialVersionUID = 1L;
	JLabel message = new JLabel();
	private ImageIcon icon = new ImageIcon(getClass().getResource("icon.png").getPath());
	private ImageIcon loss = new ImageIcon(getClass().getResource("lossL.jpg").getPath());
	private ImageIcon draw = new ImageIcon(getClass().getResource("drawL.png").getPath());
	private ImageIcon win = new ImageIcon(getClass().getResource("winL.jpg").getPath());
	
	public PopupMessage(Player winner, JFrame frame) {
		
		if(winner == Player.HUMAN) {
			setTitle("Win");
			message.setIcon(win);
		}
		else if(winner == Player.COMPUTER) {
			setTitle("Loss");
			message.setIcon(loss);
		}
		else {
			setTitle("Draw");
			message.setIcon(draw);
		}
		
		add(message);
		
		setName("Score 4");
		setIconImage(icon.getImage());
		setResizable(false);
		
		GroupLayout layout = new GroupLayout(getContentPane());
		
		getContentPane().setLayout(layout);
		
		layout.setHorizontalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
			.addComponent(message, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			)
		);
		
		layout.setVerticalGroup(
			layout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
			.addComponent(message, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE /* 10 */, GroupLayout.PREFERRED_SIZE)
			)
		);
		
		setVisible(true);
		pack();
		setLocationRelativeTo(frame);
		
	}
	
}

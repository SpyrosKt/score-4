
/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

public enum Player {

	COMPUTER(1),
	HUMAN(-1);

	private int val;

	Player(int val) {
		this.val = val;
	}

	public int getVal() {
		return val;
	}
	
	public static Player getPlayer(int val) {
		if(val == 1) return COMPUTER;
		if(val == -1) return HUMAN;
		return null;
	}

}

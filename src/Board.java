
/**
 * Spyros Koutsangelis   3130102
 * Marina Rekatsina      3140173
 * Evangeia Santorinaiou 3130182
 */

import java.util.ArrayList;
import java.util.Arrays;

public class Board implements Comparable<Board> {
	
	static final int ROWS = 6;
	static final int COLS = 7;
	
	private int table[][] = new int[ROWS][COLS];
	private int emptyCells[] = new int[COLS];
	private Player currentPlayer;
	private int value;
	private int lastMoveRow, lastMoveCol;
	
	public Board() {
		for (int i = 0; i < ROWS; i++)
			for (int j = 0; j < COLS; j++)
				table[i][j] = 0;
		
		for (int i = 0; i < COLS; i++)
			emptyCells[i] = ROWS;
		
		this.lastMoveRow = this.lastMoveCol = -1;
		
		if (Math.random() > 0.5)
			currentPlayer = Player.COMPUTER;
		else
			currentPlayer = Player.HUMAN;
		
		value = 0;
	}
	
	public Board(Board board) {
		this.table = deepCopy(board.getTable());
		this.currentPlayer = board.getCurrentPlayer();
		this.emptyCells = Arrays.copyOf(board.getEmptyCells(), COLS);
		this.lastMoveRow = board.getLastMoveRow();
		this.lastMoveCol = board.getLastMoveCol();
		this.value = board.getValue();
	}
	
	public int[][] deepCopy(int[][] table) {
		if (table == null) {
			return null;
		}
		
		int[][] newTable = new int[ROWS][];
		for (int i = 0; i < ROWS; i++) {
			newTable[i] = Arrays.copyOf(table[i], table[i].length);
		}
		return newTable;
	}
	
	public int[][] getTable() {
		return this.table;
	}
	
	public void setTable(int[][] table) {
		this.table = table;
	}
	
	public int[] getEmptyCells() {
		return emptyCells;
	}
	
	public int evaluate() {
		return value = StrictHeuristic.evaluate(this);
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}
	
	public void setCurrentPlayer(Player player) {
		this.currentPlayer = player;
	}
	
	public void changePlayer() {
		
		if (currentPlayer == Player.HUMAN)
			currentPlayer = Player.COMPUTER;
		else
			currentPlayer = Player.HUMAN;
		
	}
	
	public void insertAt(int col) {
		if (this.emptyCells[col] > 0) {
			this.table[emptyCells[col] - 1][col] = currentPlayer.getVal();
			this.lastMoveRow = emptyCells[col] - 1;
			this.lastMoveCol = col;
			this.emptyCells[col]--;
			changePlayer();
		}
	}
	
	/**
	 * Search 3 position norther than the disk's position
	 * @param row
	 * @param col
	 * @return 2 dimension array, the first row has the context of the cells, the second one zeros
	 */
	public int[][] getNorth(int row, int col) {
		// if it's near the north border, counting north cells is useless
		if (row < 3) return null;
		
		int[][] northCells = new int[4][2];
		
		for (int i = 0; i < 4; i++) {
			northCells[i][0] = table[row - i][col];
			northCells[i][1] = 0;
		}
		
		return northCells;
	}
	
	/**
	 * Search 3 position northwester than the disk's position
	 * @param row
	 * @param col
	 * @return 2 dimension array, the first row has the context of the cells, the second one the number of the
	 * supporting disk if the cell is empty.
	 */
	public int[][] getNorthWest(int row, int col) {
		if (col < 3 || row < 3) return null;
		// if it's near the north or the west border, counting north or west cells is useless
		
		int[][] northWestCells = new int[4][2];
		
		for (int i = 0; i < 4; i++) {
			northWestCells[i][0] = table[row - i][col - i];
			if(northWestCells[i][0] == 0)
				northWestCells[i][1] = emptyCells[col - i] - (row - i) - 1;
			else
				northWestCells[i][1] = 0;
		}
		
		return northWestCells;
		
	}
	
	/**
	 * Search 3 position wester than the disk's position
	 * @param row
	 * @param col
	 * @return 2 dimension array, the first row has the context of the cells, the second one the number of the
	 * supporting disk if the cell is empty.
	 */
	public int[][] getWest(int row, int col) {
		if (col < 3) return null;
		// if it's near the west border, counting west cells is useless
		
		int[][] westCells = new int[4][2];
		
		for (int i = 0; i < 4; i++) {
			westCells[i][0] = table[row][col - i];
			if(westCells[i][0] == 0)
				westCells[i][1] = emptyCells[col - i] - row - 1;
			else
				westCells[i][1] = 0;
		}
		
		return westCells;
	}
	
	/**
	 * Search 3 position southwester than the disk's position
	 * @param row
	 * @param col
	 * @return 2 dimension array, the first row has the context of the cells, the second one the number of the
	 * supporting disk if the cell is empty.
	 */
	public int[][] getSouthWest(int row, int col) {
		
		if (col < 3 || row > ROWS - 4) return null;
		// if it's near the south or the west border, counting south or west cells is useless
		
		int[][] southWestCells = new int[4][2];
		
		for (int i = 0; i < 4; i++) {
			southWestCells[i][0] = table[row + i][col - i];
			if(southWestCells[i][0] == 0)
				southWestCells[i][1] = emptyCells[col - i] - (row + i) - 1;
			else
				southWestCells[i][1] = 0;
		}
		
		return southWestCells;
	
	}
	
	/**
	 * Calculates all possible next movements and keep them in an array 
	 * @return an arraylist of all possible movements
	 */
	public ArrayList<Board> getChildren() {
		ArrayList<Board> children = new ArrayList<Board>();
		
		for (int j = 0; j < COLS; j++) {
			Board newChild = new Board(this);
			// trying to insert in a full column only creates unnecessary descendants
			if(newChild.getEmptyCells()[j] > 0) {
				newChild.insertAt(j);
				children.add(newChild);
			}
		}
		
		return children;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentPlayer == null) ? 0 : currentPlayer.hashCode());
		result = prime * result + Arrays.deepHashCode(table);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Board))
			return false;
		Board other = (Board) obj;
		if (currentPlayer != other.currentPlayer)
			return false;
		if (!Arrays.deepEquals(table, other.table))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Board b) {
		return Integer.compare(this.getValue(), b.getValue());
	}
	
	public int getLastMoveRow() {
		return lastMoveRow;
	}
	
	public int getLastMoveCol() {
		return lastMoveCol;
	}
	
	public Player getLastPlayer() {
		if(lastMoveCol == -1) 
			return null;
		
		return Player.getPlayer(table[lastMoveRow][lastMoveCol]);
	}
	
	/**
	 * Checks only if the last move made the board a terminal state
	 * @return if there is a winner
	 */
	public boolean becameTerminal() {
		for(Player p:Player.values()) {
			
			for(int i = lastMoveRow; i < ROWS; i++)
				if(getNorth(i, lastMoveCol) != null)
					if(findQuadruplet(getNorth(i, lastMoveCol), p))
						return true;
			
			for(int i = lastMoveRow, j = lastMoveCol; i < ROWS && j < COLS; i++, j++)
				if(getNorthWest(i, j) != null)
					if(findQuadruplet(getNorthWest(i, j), p))
						return true;
			
			for(int j = lastMoveCol; j < COLS; j++)
				if(getWest(lastMoveRow, j) != null)
					if(findQuadruplet(getWest(lastMoveRow, j), p))
						return true;
			
			for(int i = lastMoveRow, j = lastMoveCol; i >= 0 && j < COLS; i--, j++)
				if(getSouthWest(i, j) != null)
					if(findQuadruplet(getSouthWest(i, j), p))
						return true;
			
		}
		
		return false;
		
	}
	
	/**
	 * @return if the board is full
	 */
	public boolean isFull() {
		for(int i = 0; i < COLS; i++) 
			if(emptyCells[i] > 0)
				return false;
		return true;
	}
	
	/**
	 * @param cells
	 * @param p
	 * @return if we have quadraplet
	 */
	public boolean findQuadruplet(int[][] cells, Player p) {
		
		if (cells == null)
			return false;
		
		for (int i = 0; i < cells.length; i++) {
			
			if (cells[i][0] != p.getVal())
				return false;
		}
		return true;
	}
	
	public void print() {
		
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				if(table[i][j] == -1)
					System.out.print(" H ");
				else if(table[i][j] == 1)
					System.out.print(" C ");
				else
					System.out.print(" - ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
